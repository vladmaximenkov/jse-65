import marker.ApiCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vmaksimenkov.tm.client.TaskClient;
import ru.vmaksimenkov.tm.model.Task;

import java.util.Arrays;
import java.util.List;

public class TaskTest {

    @Test
    @Category(ApiCategory.class)
    public void test() {
        @NotNull final TaskClient client = new TaskClient();
        client.delete();
        Assert.assertTrue(client.get().isEmpty());
        @NotNull final Task task = new Task("test");
        client.post(task);
        Assert.assertNotNull(client.get());
        task.setName("test test");
        client.put(task);
        Assert.assertEquals("test test", client.get(task.getId()).getName());
        client.delete(task.getId());
        Assert.assertTrue(client.get().isEmpty());
        @NotNull final Task task2 = new Task("try");
        @NotNull List<Task> list = Arrays.asList(task, task2);
        client.post(list);
        Assert.assertEquals(2, client.get().size());
        task.setName("try test");
        task2.setName("test and try");
        list = Arrays.asList(task, task2);
        client.put(list);
        Assert.assertEquals("test and try", client.get(task2.getId()).getName());
        Assert.assertEquals("try test", client.get(task.getId()).getName());
    }

}
