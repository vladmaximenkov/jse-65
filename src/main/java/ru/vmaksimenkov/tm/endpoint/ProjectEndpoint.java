package ru.vmaksimenkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.api.resource.IProjectResource;
import ru.vmaksimenkov.tm.model.Project;
import ru.vmaksimenkov.tm.service.ProjectService;

@RestController
@RequestMapping(value = "/api/project", produces = "application/json")
public class ProjectEndpoint implements IProjectResource {

    @NotNull
    private final ProjectService service;

    @Autowired
    public ProjectEndpoint(@NotNull final ProjectService service) {
        this.service = service;
    }

    @Override
    @GetMapping("/{id}")
    public Project get(@NotNull @PathVariable("id") final String id) {
        return service.findById(id);
    }

    @Override
    @PostMapping
    public void post(@NotNull @RequestBody final Project project) {
        service.merge(project);
    }

    @Override
    @PutMapping
    public void put(@NotNull @RequestBody final Project project) {
        service.merge(project);
    }

    @Override
    @DeleteMapping("/{id}")
    public void delete(@NotNull @PathVariable("id") final String id) {
        service.removeById(id);
    }

}
