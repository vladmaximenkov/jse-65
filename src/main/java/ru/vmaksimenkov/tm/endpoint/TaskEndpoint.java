package ru.vmaksimenkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.api.resource.ITaskResource;
import ru.vmaksimenkov.tm.model.Task;
import ru.vmaksimenkov.tm.service.TaskService;

@RestController
@RequestMapping(value = "/api/task", produces = "application/json")
public class TaskEndpoint implements ITaskResource {

    @NotNull
    private final TaskService service;

    @Autowired
    public TaskEndpoint(@NotNull final TaskService service) {
        this.service = service;
    }

    @Override
    @GetMapping("/{id}")
    public Task get(@NotNull @PathVariable("id") final String id) {
        return service.findById(id);
    }

    @Override
    @PostMapping
    public void post(@NotNull @RequestBody final Task task) {
        service.merge(task);
    }

    @Override
    @PutMapping
    public void put(@NotNull @RequestBody final Task task) {
        service.merge(task);
    }

    @Override
    @DeleteMapping("/{id}")
    public void delete(@NotNull @PathVariable("id") final String id) {
        service.removeById(id);
    }

}
