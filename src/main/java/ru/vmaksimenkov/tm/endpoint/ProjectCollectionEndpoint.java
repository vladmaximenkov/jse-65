package ru.vmaksimenkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.api.resource.IProjectCollectionResource;
import ru.vmaksimenkov.tm.model.Project;
import ru.vmaksimenkov.tm.service.ProjectService;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping(value = "/api/projects", produces = "application/json")
public class ProjectCollectionEndpoint implements IProjectCollectionResource {

    @NotNull
    private final ProjectService service;

    @Autowired
    public ProjectCollectionEndpoint(@NotNull final ProjectService service) {
        this.service = service;
    }

    @Override
    @GetMapping
    public Collection<Project> get() { return service.findAll(); }


    @Override
    @PostMapping
    public void post(@NotNull @RequestBody final List<Project> projects) {
        service.merge(projects);
    }


    @Override
    @PutMapping
    public void put(@NotNull @RequestBody final List<Project> projects) {
        service.merge(projects);
    }


    @Override
    @DeleteMapping
    public void delete() {
        service.removeAll();
    }

}
