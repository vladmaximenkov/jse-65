package ru.vmaksimenkov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
public enum Status {

    NOT_STARTED("NOT STARTED"),
    IN_PROGRESS("IN PROGRESS"),
    COMPLETED("COMPLETED");

    @NotNull
    private final String displayName;

    Status(@NotNull final String displayName) { this.displayName = displayName; }

}
