package ru.vmaksimenkov.tm.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.client.RestTemplate;
import ru.vmaksimenkov.tm.api.resource.ITaskCollectionResource;
import ru.vmaksimenkov.tm.api.resource.ITaskResource;
import ru.vmaksimenkov.tm.model.Task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ru.vmaksimenkov.tm.constant.Const.*;

public class TaskClient implements ITaskResource, ITaskCollectionResource {

    public void post(@NotNull final List<Task> tasks) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForEntity(TASKS, tasks, void.class);
    }

    @NotNull
    public List<Task> get() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @Nullable final Task[] tasks = restTemplate.getForObject(TASKS, Task[].class);
        if (tasks == null) return new ArrayList<>();
        return Arrays.asList(tasks);
    }

    public void put(@NotNull final List<Task> tasks) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.put(TASKS, tasks, void.class);
    }

    public void delete() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(TASKS);
    }

    public void post(@NotNull final Task task) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForEntity(TASK, task, void.class);
    }

    @Nullable
    public Task get(@NotNull final String id) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(TASK+"/{id}", Task.class, id);
    }

    public void put(@NotNull final Task task) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.put(TASK, task, void.class);
    }

    public void delete(@NotNull final String id) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(TASK+"/{id}", id);
    }

}
