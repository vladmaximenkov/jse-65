package ru.vmaksimenkov.tm.api.resource;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectCollectionResource {

    @NotNull
    @GetMapping
    Collection<Project> get();

    @PostMapping
    void post(@NotNull @RequestBody List<Project> projects);

    @PutMapping
    void put(@NotNull @RequestBody List<Project> projects);

    @DeleteMapping
    void delete();

}
