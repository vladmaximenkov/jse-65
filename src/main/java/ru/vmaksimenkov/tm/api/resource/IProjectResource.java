package ru.vmaksimenkov.tm.api.resource;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.model.Project;

public interface IProjectResource {

    @GetMapping("/{id}")
    Project get(@NotNull @PathVariable("id") String id);

    @PostMapping
    void post(@NotNull @RequestBody Project project);

    @PutMapping
    void put(@NotNull @RequestBody Project project);

    @DeleteMapping("/{id}")
    void delete(@NotNull @PathVariable("id") String id);

}
