package ru.vmaksimenkov.tm.api.resource;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.model.Task;

public interface ITaskResource {

    @GetMapping("/{id}")
    Task get(@NotNull @PathVariable("id") String id);

    @PostMapping
    void post(@NotNull @RequestBody Task task);

    @PutMapping
    void put(@NotNull @RequestBody Task task);

    @DeleteMapping("/{id}")
    void delete(@NotNull @PathVariable("id") String id);

}
